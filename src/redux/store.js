import { configureStore } from "@reduxjs/toolkit";
import products from "./reducers/products.js";
import cart from "./reducers/cart.js";

const store = configureStore({
  reducer: {
    products,
    cart,
  },
});

export default store;
