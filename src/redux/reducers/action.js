import {
  ADD_PRODUCT,
  ADD_TO_CART,
  DELETE_PRODUCT,
  INITIAL_PRODUCTS,
  REMOVE_PRODUCT,
  UPDATE_PRODUCT,
} from "./actionTypes";

export const initialiseProducts = (products) => {
  return {
    type: INITIAL_PRODUCTS,
    payload: products,
  };
};

export const removeProduct = (productId) => {
  return {
    type: REMOVE_PRODUCT,
    payload: productId,
  };
};

export const addProduct = (oneProduct, id) => {
  return {
    type: ADD_PRODUCT,
    payload: [oneProduct, id],
  };
};

export const updateProduct = (oneProduct) => {
  return {
    type: UPDATE_PRODUCT,
    payload: oneProduct,
  };
};

export const addToCart = (oneProduct) => {
  return {
    type: ADD_TO_CART,
    payload: oneProduct,
  };
};

export const deleteCartItem = (productId) => {
  return {
    type: DELETE_PRODUCT,
    payload: productId,
  };
};
