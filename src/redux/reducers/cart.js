import { ADD_TO_CART, DELETE_PRODUCT } from "./actionTypes";

const initialState = {
  productsGot: [],
};

const cart = (state = initialState, action) => {
  const productsGot = action.payload;
  console.log("ProductGOT", productsGot);
  switch (action.type) {
    case ADD_TO_CART:
      let newProducts = [action.payload, ...state.productsGot];
      console.log("New Products", newProducts);
      return {
        ...state,
        productsGot: newProducts,
      };
    case DELETE_PRODUCT:
      console.log("Inside Delete Product Condition", state.productsGot);
      let productsAfterDelete = state.productsGot.filter((eachProduct) => {
        return eachProduct.id !== action.payload;
      });
      return {
        ...state,
        productsGot: productsAfterDelete,
      };
    default:
      return state;
  }
};

export default cart;
