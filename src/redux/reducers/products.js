import {
  ADD_PRODUCT,
  INITIAL_PRODUCTS,
  REMOVE_PRODUCT,
  UPDATE_PRODUCT,
} from "./actionTypes";

const initialState = {
  productsGot: [],
};

const products = (state = initialState, action) => {
  const productsGot = action.payload;
  switch (action.type) {
    case INITIAL_PRODUCTS:
      return {
        ...state,
        productsGot,
      };
    case REMOVE_PRODUCT:
      return {
        ...state,
        productsGot: state.productsGot.filter((eachProduct) => {
          return eachProduct.id !== action.payload;
        }),
      };
    case ADD_PRODUCT:
      const newProduct = {
        ...action.payload[0],
        id: action.payload[1],
        rating: {
          rate: 0,
          count: 0,
        },
      };
      let newProductsOnAddition = [...state.productsGot, newProduct];
      return {
        ...state,
        productsGot: newProductsOnAddition,
      };
    case UPDATE_PRODUCT:
      if (state.productsGot === undefined) {
        return;
      }
      const newProductsFiltering = state.productsGot.filter((eachData) => {
        return eachData.id.toString() !== action.payload.id.toString();
      });
      let newProductsOnUpdation = [action.payload, ...newProductsFiltering];
      return {
        ...state,
        productsGot: newProductsOnUpdation,
      };
    default:
      return state;
  }
};

export default products;
