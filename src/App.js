//Importing npm packages
import axios from "axios";
import React, { Component } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { connect } from "react-redux";

//Importing other files
import "./App.css";
import Product from "./components/Product.js";
import ProductAddForm from "./components/ProductAddForm.js";
import ProductUpdateForm from "./components/ProductUpdateForm.js";
import Cart from "./components/Cart";
import { INITIAL_PRODUCTS } from "./redux/reducers/actionTypes";

class App extends Component {
  constructor(props) {
    super(props);

    this.API_STATES = {
      LOADING: "loading",
      LOADED: "loaded",
      ERROR: "error",
    };

    this.state = {
      API_STATUS: this.API_STATES.LOADING,
    };
  }

  componentDidMount() {
    //To Fetch all the data from API
    axios
      .get("https://fakestoreapi.com/products/")
      .then((dataGot) => {
        this.props.initialProducts(dataGot.data);
        this.setState({
          ...this.state,
          API_STATUS: this.API_STATES.LOADED,
        });
      })
      .catch((errorMessage) => {
        this.setState({ API_STATUS: this.API_STATES.ERROR });
      });
  }

  render() {
    // Making all the Routes and sending props
    return (
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={
              <Product
                API_STATUS={this.state.API_STATUS}
                API_STATES={this.API_STATES}
                dataRecieved={this.props.products}
                updateItem={this.updateItemClicked}
              />
            }
          />
          <Route
            path="/Cart"
            element={<Cart allProducts={this.props.cartProducts} />}
          />
          <Route path="/AddItem" element={<ProductAddForm />} />
          <Route
            path="/UpdateItem/:productId"
            element={
              <ProductUpdateForm
                updateItem={this.updateItemClicked}
                allProducts={this.props.products}
              />
            }
          />
        </Routes>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = (state) => {
  const products = state.products.productsGot;
  console.log("StateMapProduct", products);
  const cartProducts = state.cart.productsGot;
  console.log("StateMapCart", cartProducts);
  return {
    products,
    cartProducts,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    initialProducts: (data) =>
      dispatch({ type: INITIAL_PRODUCTS, payload: data }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
