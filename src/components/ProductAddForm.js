import React, { Component } from "react";
import { v4 as uuidv4 } from "uuid";

import Navbar from "./layout/Navbar";
import Footer from "./layout/Footer";
import { connect } from "react-redux";
import { ADD_PRODUCT } from "../redux/reducers/actionTypes";

class ProductAddForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      item: {
        id: "",
        title: "",
        description: "",
        price: "",
        category: "",
        image: "",
      },
      itemAdded: "not_added",
    };
  }

  handleChange = (event) => {
    let { name, value } = event.target;

    this.setState({
      item: {
        ...this.state.item,
        [name]: value,
      },
    });
  };

  handleEvent = (event) => {
    event.preventDefault();
    let newId = uuidv4();
    this.props.addProduct(this.state.item, newId);
    this.setState({ itemAdded: "added" });
    setTimeout(() => {
      this.setState({ itemAdded: "not_added" });
    }, 5 * 1000);
  };

  render() {
    return (
      <>
        <Navbar />
        <div className="formContainer">
          <form onSubmit={this.handleEvent}>
            <div className="input-group mb-3">
              <div className="input-group-prepend">
                <span
                  className="input-group-text"
                  id="inputGroup-sizing-default"
                >
                  Title
                </span>
              </div>
              <input
                type="text"
                className="form-control"
                aria-label="Default"
                aria-describedby="inputGroup-sizing-default"
                onChange={(event) => {
                  this.handleChange(event);
                }}
                name="title"
                required
              />
            </div>
            <div className="input-group mb-3">
              <div className="input-group-prepend">
                <span
                  className="input-group-text"
                  id="inputGroup-sizing-default"
                >
                  Category
                </span>
              </div>
              <input
                type="text"
                className="form-control"
                aria-label="Default"
                aria-describedby="inputGroup-sizing-default"
                name="category"
                onChange={(event) => {
                  this.handleChange(event);
                }}
                required
              />
            </div>
            <div className="input-group">
              <div className="input-group-prepend">
                <span className="input-group-text">Description</span>
              </div>
              <textarea
                className="form-control"
                aria-label="With textarea"
                onChange={(event) => {
                  this.handleChange(event);
                }}
                name="description"
                required
              ></textarea>
            </div>
            <div className="input-group mb-3">
              <div className="input-group-prepend">
                <span className="input-group-text">$</span>
              </div>
              <input
                type="text"
                className="form-control"
                aria-label="Amount (to the nearest dollar)"
                onChange={(event) => {
                  this.handleChange(event);
                }}
                name="price"
                required
              />
            </div>
            <div className="input-group mb-3">
              <div className="input-group-prepend">
                <span
                  className="input-group-text"
                  id="inputGroup-sizing-default"
                >
                  Image URL
                </span>
              </div>
              <input
                type="text"
                className="form-control"
                aria-label="Default"
                aria-describedby="inputGroup-sizing-default"
                onChange={(event) => {
                  this.handleChange(event);
                }}
                name="image"
                required
              />
            </div>
            <button type="submit" className="btn btn-primary">
              Add Item
            </button>
            {this.state.itemAdded === "added" && (
              <div className="text-success">
                Item is added, See the last Item in the Menu!
              </div>
            )}
          </form>
        </div>
        <Footer />
      </>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addProduct: (oneProduct, id) =>
      dispatch({
        type: ADD_PRODUCT,
        payload: [oneProduct, id],
      }),
  };
};

export default connect(null, mapDispatchToProps)(ProductAddForm);
