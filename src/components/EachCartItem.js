/* eslint-disable jsx-a11y/img-redundant-alt */
import React from "react";
import { connect } from "react-redux";
import { DELETE_PRODUCT } from "../redux/reducers/actionTypes";

class EachCartItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      quantity: 1,
      Error_Remove: "N0_ERROR",
    };
  }

  addItem = () => {
    this.setState({ quantity: this.state.quantity + 1 });
  };

  deleteItem = (id) => {
    this.props.deleteCartItem(id);
  };

  removeItem = () => {
    if (this.state.quantity > 1) {
      this.setState({
        quantity: this.state.quantity - 1,
        Error_Remove: "NO_ERROR",
      });
    } else {
      this.setState({
        Error_Remove: "ERROR",
      });
    }
  };

  render() {
    const { image, title, price, id } = this.props.eachCartItem;
    return (
      <div className="cartItemContainer">
        <div className="cartDetailcol">
          <img src={image} alt="No Image Available" className="cartItemImage" />
        </div>
        <div className="cartDetailcol">{title}</div>
        <div className="cartDetailcol">
          <i className="fa-solid fa-circle-plus" onClick={this.addItem}></i>
          {this.state.quantity}
          <i className="fa-solid fa-circle-minus" onClick={this.removeItem}></i>
        </div>
        <div className="cartDetailcol">{price}</div>
        <div className="cartDetailcol">{price * this.state.quantity}</div>
        <div className="cartDetailcol">
          <i
            className="fa-solid fa-trash"
            onClick={() => {
              return this.deleteItem(id);
            }}
          ></i>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    deleteCartItem: (productId) =>
      dispatch({
        type: DELETE_PRODUCT,
        payload: productId,
      }),
  };
};

export default connect(null, mapDispatchToProps)(EachCartItem);

// export default EachCartItem;
