import React, { Component } from "react";

import AllItems from "./AllItems";
import Navbar from "./layout/Navbar";
import Footer from "./layout/Footer";

class Product extends Component {
  render() {
    let { API_STATUS, dataRecieved } = this.props;
    return (
      <>
        <Navbar />
        {API_STATUS === this.props.API_STATES.LOADING && (
          <div className="lds-ring">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        )}
        {API_STATUS === this.props.API_STATES.ERROR && (
          <h1 className="message">Internal Server Error</h1>
        )}
        {API_STATUS === this.props.API_STATES.LOADED &&
          dataRecieved.length === 0 && (
            <h1 className="message">No product available at this moment.</h1>
          )}
        {API_STATUS === this.props.API_STATES.LOADED &&
          dataRecieved.length !== 0 && (
            <AllItems allItemDetails={dataRecieved} />
          )}
        <Footer />
      </>
    );
  }
}

export default Product;
