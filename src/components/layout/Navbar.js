import React, { Component } from "react";
import { Link } from "react-router-dom";

class Navbar extends Component {
  render() {
    return (
      <nav>
        <Link to="/">
          <h1>Do-Shop</h1>
        </Link>
        <div>
          <Link to="/Cart">
            <input
              type="button"
              value="Your Cart"
              className="addButton"
            ></input>
          </Link>
          <Link to="/AddItem">
            <input type="button" value="Add Item" className="addButton"></input>
          </Link>
        </div>
      </nav>
    );
  }
}

export default Navbar;
