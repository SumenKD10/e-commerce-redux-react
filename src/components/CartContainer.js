import React from "react";
import EachCartItem from "./EachCartItem";

class CartContainer extends React.Component {
  render() {
    return (
      <div className="cartContainer">
        <h1>Your Cart</h1>
        <div className="cartHeader">
          <div>All Items</div>
          <div>Price</div>
        </div>
        {this.props.allCarts.map((eachItem) => {
          return (
            <EachCartItem key={eachItem.productId} eachCartItem={eachItem} />
          );
        })}
      </div>
    );
  }
}

export default CartContainer;
