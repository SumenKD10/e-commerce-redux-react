/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import {
  ADD_TO_CART,
  DELETE_PRODUCT,
  REMOVE_PRODUCT,
} from "../redux/reducers/actionTypes";

class EachItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      itemAdded: false,
      itemExist: false,
    };
  }

  handleClickDelete = (id) => {
    this.props.deleteCartItem(id);
    this.props.removeProducts(id);
  };

  handleAddProduct = (product) => {
    let allProductsIds = this.props.cartProducts.map((eachItem) => {
      return eachItem.id;
    });

    if (!allProductsIds.includes(product.id)) {
      this.props.addToCart(product);
      this.setState({ itemAdded: true, itemExist: false });
    } else {
      this.setState({ itemAdded: false, itemExist: true });
    }
  };

  render() {
    let { id, category, image, title, description, price, rating } =
      this.props.eachItemDetail;
    return (
      <div>
        <div className="itemCard">
          <p className="category">📍{category}</p>
          <img className="eachItemImage" src={image} alt="No Image Available" />
          <div className="ratingsAndCount">
            <p>⭐{rating.rate}</p>
            <p>👤{rating.count}</p>
          </div>
          <div className="card-body">
            <h5 className="card-title">{title}</h5>
            <p className="card-text">{description.slice(0, 200)}</p>
          </div>
          <div className="card-footer">
            <small className="card-price">${price}</small>
            <Link to={`/UpdateItem/${id}`}>
              <i
                className="fa-solid fa-square-pen"
                onClick={() => {
                  return this.handleClickUpdate;
                }}
              ></i>
            </Link>
            <i
              className="fa-sharp fa-solid fa-trash"
              onClick={() => {
                this.handleClickDelete(id);
              }}
            ></i>
            <input
              type="button"
              value="Add To Cart"
              onClick={() => {
                return this.handleAddProduct(this.props.eachItemDetail);
              }}
            />
          </div>
          {this.state.itemAdded && <div className="successAdded">Added</div>}
          {this.state.itemExist && (
            <div className="alreadyAdded">Already Added</div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const cartProducts = state.cart.productsGot;
  console.log("StateMapCart", cartProducts);
  return {
    cartProducts,
  };
};

const mapToDispatchProps = (dispatch) => {
  return {
    removeProducts: (productId) =>
      dispatch({
        type: REMOVE_PRODUCT,
        payload: productId,
      }),
    addToCart: (product) =>
      dispatch({
        type: ADD_TO_CART,
        payload: product,
      }),
    deleteCartItem: (productId) =>
      dispatch({
        type: DELETE_PRODUCT,
        payload: productId,
      }),
  };
};

export default connect(mapStateToProps, mapToDispatchProps)(EachItem);
