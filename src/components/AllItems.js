import React, { Component } from "react";

import EachItem from "./EachItem.js";

class AllItems extends Component {
  render() {
    return (
      <div className="itemCardContainer">
        {this.props.allItemDetails.map((eachItem) => {
          return <EachItem key={eachItem.id} eachItemDetail={eachItem} />;
        })}
      </div>
    );
  }
}

export default AllItems;
