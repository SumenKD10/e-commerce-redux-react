import React, { Component } from "react";

import Navbar from "./layout/Navbar";
import Footer from "./layout/Footer";
import { connect } from "react-redux";
import { UPDATE_PRODUCT } from "../redux/reducers/actionTypes";

class ProductUpdateForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      productFound: "",
      itemUpdated: "not_updated",
    };
  }

  componentDidMount() {
    const id = window.location.href.split("/").slice(4);
    console.log("Props", this.props);
    let foundProduct = this.props.allProducts.find((eachProduct) => {
      return eachProduct.id.toString() === id.toString();
    });
    this.setState({
      productFound: foundProduct,
    });
  }

  handleChange = (event) => {
    let { name, value } = event.target;

    this.setState((prevState) => ({
      productFound: { ...prevState.productFound, [name]: value },
    }));
  };

  handleEvent = (event) => {
    event.preventDefault();
    this.props.updateProduct(this.state.productFound);
    this.setState({ itemUpdated: "updated" });
    setTimeout(() => {
      this.setState({ itemUpdated: "not_updated" });
    }, 5 * 1000);
  };

  render() {
    return (
      <>
        <Navbar />
        {this.state.productFound === undefined ? (
          <h1 className="message">back to home page</h1>
        ) : (
          <div className="formContainer">
            <form onSubmit={this.handleEvent}>
              <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <span
                    className="input-group-text"
                    id="inputGroup-sizing-default"
                  >
                    Title
                  </span>
                </div>
                <input
                  type="text"
                  className="form-control"
                  aria-label="Default"
                  aria-describedby="inputGroup-sizing-default"
                  onChange={(event) => {
                    this.handleChange(event);
                  }}
                  value={this.state.productFound.title}
                  name="title"
                  required
                />
              </div>
              <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <span
                    className="input-group-text"
                    id="inputGroup-sizing-default"
                  >
                    Category
                  </span>
                </div>
                <input
                  type="text"
                  className="form-control"
                  aria-label="Default"
                  aria-describedby="inputGroup-sizing-default"
                  name="category"
                  onChange={(event) => {
                    this.handleChange(event);
                  }}
                  value={this.state.productFound.category}
                  required
                />
              </div>
              <div className="input-group">
                <div className="input-group-prepend">
                  <span className="input-group-text">Description</span>
                </div>
                <textarea
                  className="form-control"
                  aria-label="With textarea"
                  onChange={(event) => {
                    this.handleChange(event);
                  }}
                  name="description"
                  value={this.state.productFound.description}
                  required
                ></textarea>
              </div>
              <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <span className="input-group-text">$</span>
                </div>
                <input
                  type="text"
                  className="form-control"
                  aria-label="Amount (to the nearest dollar)"
                  onChange={(event) => {
                    this.handleChange(event);
                  }}
                  name="price"
                  value={this.state.productFound.price}
                  required
                />
              </div>
              <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <span
                    className="input-group-text"
                    id="inputGroup-sizing-default"
                  >
                    Image URL
                  </span>
                </div>
                <input
                  type="text"
                  className="form-control"
                  aria-label="Default"
                  aria-describedby="inputGroup-sizing-default"
                  onChange={(event) => {
                    this.handleChange(event);
                  }}
                  name="image"
                  value={this.state.productFound.image}
                  required
                />
              </div>
              <button type="submit" className="btn btn-primary">
                Update Item
              </button>
              {this.state.itemUpdated === "updated" && (
                <div className="text-success">
                  Item is updated, See the first Item in the Menu!
                </div>
              )}
            </form>
          </div>
        )}
        <Footer />
      </>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    updateProduct: (oneProduct) => {
      dispatch({ type: UPDATE_PRODUCT, payload: oneProduct });
    },
  };
};

export default connect(null, mapDispatchToProps)(ProductUpdateForm);
