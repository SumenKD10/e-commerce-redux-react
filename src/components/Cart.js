import Navbar from "./layout/Navbar.js";
import Footer from "./layout/Footer.js";
import CartContainer from "./CartContainer.js";
import React from "react";

class Cart extends React.Component {
  render() {
    console.log("ALL", this.props.allProducts);
    return (
      <>
        <Navbar />
        {this.props.allProducts.length === 0 && (
          <h1 className="message">No Products Available!</h1>
        )}
        {this.props.allProducts.length !== 0 && (
          <CartContainer allCarts={this.props.allProducts} />
        )}
        <Footer />
      </>
    );
  }
}

export default Cart;
